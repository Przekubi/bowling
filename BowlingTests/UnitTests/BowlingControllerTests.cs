﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Web.Mvc;
using Bowling.DAL;
using System.Net;
using NSubstitute;
using System.Web;

namespace Bowling.Controllers.Tests
{
    [TestClass]
    public class BowlingControllerTests
    {
        BowlingController fakeController;
        IFileManager fakeFileManager;
        [TestInitialize]
        public void Initialize()
        {
            fakeFileManager = Substitute.For<IFileManager>();
            fakeController = new BowlingController(fakeFileManager);
            
        }

        [TestMethod]
        public void Index_NoParametersOverload_ReturnBowlingViewModelType()
        {
            //arrange

            //act
            var result = fakeController.Index() as ViewResult;

            //assert
            Assert.AreEqual(result.Model.GetType(), typeof(BowlingViewModel));
        }

        [TestMethod]
        public void Index_PostSendWithoutFile_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel.UploadedFile = null;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Index_PostSendFileBadContentType_ReturnHttpStatusCodeForbidden()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("error");
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as HttpStatusCodeResult;

            //assert
            Assert.AreEqual(result.StatusCode, (int)HttpStatusCode.Forbidden);

        }

        [TestMethod]
        public void Index_PostSendFileExceptionWhenReadStream_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            fakeFileManager.When(o=>o.ConvertTxtFileToString(fakeFile)).Throw(new Exception());
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);
        }


        [TestMethod]
        public void Index_PostSendFileBadContent_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}Test2",Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result =fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Index_SentScoreExceed10_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}12,2,0,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);

        }

        [TestMethod]
        public void Index_SentSumOfTwoScoresInSameRoundExceed10_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}4,9,0,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);

        }

        [TestMethod]
        public void Index_StrikeIsBadMarked_ModelStateNotValid()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}10,0,0,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.IsFalse(result.ViewData.ModelState.IsValid);

        }


        [TestMethod]
        public void Index_CalculateScoreWithNoStrikesAndSpares_ReturnViewModelWithNormalCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}1,2,0,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 6);
        }

        [TestMethod]
        public void Index_CalculateScoreWithStrike_ReturnViewModelWithStrikeCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}10,-,1,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 16);

        }

        [TestMethod]
        public void Index_CalculateScoreWithTwoStrikes_ReturnViewModelWithTwoStrikesCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}10,-,10,-,2,2", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 38);

        }

        [TestMethod]
        public void Index_CalculateScoreWithSpare_ReturnViewModelWithSpareCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}5,5,2,1,2,0", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 17);

        }

        [TestMethod]
        public void Index_CalculateScoreWithTwoSpares_ReturnViewModelWithSpareCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}5,5,6,4,2,1", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 31);
            
        }

        [TestMethod]
        public void Index_CalculateScoreWithSpareAndStrike_ReturnViewModelWithSpareAndStrikeCalculatedScore()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}5,5,6,4,2,1", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            var player = ((BowlingViewModel)result.Model).Players.SingleOrDefault();
            Assert.AreEqual(player.TotalScore, 31);

        }

        [TestMethod]
        public void Index_CalculateScoreWithTwoPlayers_ReturnViewModelTwoPlayers()
        {
            //arrange
            BowlingViewModel fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            fakeBowlingViewModel = Substitute.For<BowlingViewModel>();
            var fakeFile = Substitute.For<HttpPostedFileBase>();
            fakeFile.ContentType.Returns("text/plain");
            string fakeContent = string.Format("Test{0}5,5,6,4,2,1{0}Test2{0}3,4,6,2,1,5", Environment.NewLine);
            fakeFileManager.ConvertTxtFileToString(fakeFile).Returns(fakeContent);
            fakeBowlingViewModel.UploadedFile = fakeFile;

            //act
            var result = fakeController.Index(fakeBowlingViewModel) as ViewResult;

            //assert
            Assert.AreEqual(((BowlingViewModel)result.Model).Players.Count(), 2);

        }
    }
}