﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bowling.Models
{
    public class Player
    {
        public string Name { get; set; }
        public int[] Score { get; set; }
        public int TotalScore { get; set; }
    }
}