﻿using Bowling.DAL;
using Bowling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Bowling.Controllers
{
    public class BowlingController : Controller
    {
        IFileManager _fileManager;
        public BowlingController(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }
        // GET: Bowling
        public ActionResult Index()
        {
            BowlingViewModel viewModel = new BowlingViewModel();
            return View(viewModel);
        }

        /// <summary>
        /// Controller that using sent text file to calculate bowling score
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(BowlingViewModel viewModel)
        {
            //check if user sent file
            if (viewModel.UploadedFile == null)
            {
                ModelState.AddModelError("ErrorMessage", "Nie wybrano pliku!");
                return View(viewModel);
            }
            //check if type of file is correct
            if (viewModel.UploadedFile.ContentType!="text/plain")
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            try
            {
                //read sent file and recive string
                string textFromFile = _fileManager.ConvertTxtFileToString(viewModel.UploadedFile);
                //get calculated score
                viewModel.Players = GetPlayersAndScoreFromText(textFromFile);
                //get to viewBag maximum scores player
                if (viewModel.Players != null)
                {
                    ViewBag.MaxScoreCount = viewModel.Players.Select(o => o.Score).Select(o => o.Count()).Max();
                }

            }
            catch
            {
                ModelState.AddModelError("ErrorMessage", "Błąd odczytu pliku");
            }

            return View(viewModel);
        }

        //method that convert string to player class data
        private IEnumerable<Player> GetPlayersAndScoreFromText(string text)
        {
            List<Player> listOfPlayers = new List<Player>();
            //create list of players from string that is specifed from instruction
            string[] lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                
                for(int i = 0; i < lines.Count(); i=i+2)
                {
                    int[] scoreArray = ConvertScoreStringToScoreIntArray(lines[i + 1]);
                    if (scoreArray != null)
                    {
                        //if everything all right, create and add new player to list
                        Player player = new Player()
                        {
                            Name = lines[i],
                            Score = scoreArray,
                            TotalScore = CalculateRoundTotalScore(scoreArray)
                        };
                        listOfPlayers.Add(player);

                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch
            {
                ModelState.AddModelError("ErrorMessage", "Dokument niepoprawnie skonstruowany! Upewnij się że wygląda on wg wzoru!");
                return null;
            }
            
            return listOfPlayers;
        }
        //method for counting score input 
        private int[] ConvertScoreStringToScoreIntArray(string convertedString)
        {
            //split every score basing on comma
            var everyScoreArrayString = convertedString.Split(new[] { ',' } , StringSplitOptions.RemoveEmptyEntries);
            var arrayOfScores = new int[everyScoreArrayString.Length];
            try
            {
                //because of existence of strikes iterate array by 2
                for (int i = 0; i < everyScoreArrayString.Count(); i = i + 2)
                {
                    int firstScore;
                    if (int.TryParse(everyScoreArrayString[i], out firstScore))
                    {
                        // check if score exceed permitted values 
                        if (firstScore > 10)
                        {
                            ModelState.AddModelError("ErrorMessage", "Wartość przynajmniej jednego z wyników przekracza 10. Upewnij się że dane są dobrze wprowadzone");
                            return null;
                        }
                        arrayOfScores[i]=firstScore;
                    }
                    else
                    {
                        throw new Exception();
                    }
                    //check if the firsc score is strike 
                    if (firstScore == 10)
                    {
                        //check if strike is properly marked
                        if (everyScoreArrayString[i + 1] != "-")
                        {
                            ModelState.AddModelError("ErrorMessage", "Wartość po strike'u jest źle zaznaczona!");
                            return null;
                        }
                        continue;
                    }
                    if (everyScoreArrayString.Count() > i + 1)
                    {
                        int secondScore;
                        if (int.TryParse(everyScoreArrayString[i + 1] , out secondScore))
                        {
                            // check if score exceed permitted values
                            if (secondScore > 10 || firstScore+secondScore>10)
                            {
                                ModelState.AddModelError("ErrorMessage", "Wartość przynajmniej jednego z wyników przekracza 10. Upewnij się że dane są dobrze wprowadzone");
                                return null;
                            }
                            else if(firstScore + secondScore > 10)
                            {
                                ModelState.AddModelError("ErrorMessage", "Wynik w jednej rundzie przekracza 10. Upewnij się że dane są dobrze wprowadzone");
                                return null;
                            }
                            arrayOfScores[i + 1]=secondScore;
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }
            }
            catch
            {
                throw new Exception();
            }
            return arrayOfScores;
            
        }
        
        //mehtod that get score array of bowling rounds and calculate total score basing of rules of bowling
        private int CalculateRoundTotalScore(int[] scoreArray)
        {
            //first rund always start from zero and with normal counitng rules
            int sum = 0;
            PlayStatus statusFlag = PlayStatus.Normal;
            for(int i=0; i< scoreArray.Length; i=i+2)
            {
                int firstRoundScore = scoreArray[i];
                int secondRoundScore = 0;
                if (scoreArray.Length > i + 1)
                {
                    secondRoundScore = scoreArray[i + 1];
                }
                //check counting rules from previous round
                switch (statusFlag)
                {
                    case (PlayStatus.Strike):
                        sum += (firstRoundScore + secondRoundScore) * 2;
                        break;
                    case (PlayStatus.Spare):
                        sum += (firstRoundScore * 2) + secondRoundScore;
                        break;
                    default:
                        sum += firstRoundScore + secondRoundScore;
                        break;

                }
                //check if strike occurs
                if (firstRoundScore == 10)
                {
                    statusFlag=PlayStatus.Strike;
                }
                //check if spare occurs
                else if (firstRoundScore + secondRoundScore == 10)
                {
                    statusFlag = PlayStatus.Spare;
                }
                //if nothing occurs, set status to normal counting
                else
                {
                    statusFlag = PlayStatus.Normal;
                }
            }
            
            return sum;
        }
    }
}