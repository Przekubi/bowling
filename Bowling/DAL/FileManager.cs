﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Bowling.DAL
{
    public class FileManager : IFileManager
    {
        public string ConvertTxtFileToString(HttpPostedFileBase file)
        {
            string text;
            using (StreamReader reader = new StreamReader(file.InputStream))
            {
                 text= reader.ReadToEnd();
            }
            return text;
        }
    }
}