﻿using Bowling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bowling.DAL
{
    public class BowlingViewModel
    {
        public HttpPostedFileBase UploadedFile { get; set; }
        public IEnumerable<Player> Players { get; set; }
    }
}