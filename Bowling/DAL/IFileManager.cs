﻿using System.Web;

namespace Bowling.DAL
{
    public interface IFileManager
    {
        string ConvertTxtFileToString(HttpPostedFileBase file);
    }
}